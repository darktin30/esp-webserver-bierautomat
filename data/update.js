let drinks = ["teeole-count", "raspa-count", "mexi-count", "wasser-count", "hopfengold-count", "landbier-count", "radler-count"];

setInterval(function() 
{
  updateCounters();
  updateTemp();
}, 2000); 

function setData(id, data) {
  if (!(id.localeCompare("undefined")==0)) {
    document.getElementById(id).innerHTML = data;
  }
}

function updateTemp()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var temp = this.responseText.replace(/.$/, '') +" °C";
      setData("temperature", temp);
    }
  };
  xhttp.open("GET", "temp", true);
  xhttp.send();
}

function parseStatus(status)
{
  var drinkDict = {};

  const slots = status.split(";");
  for (const slot of slots) {
  	const drink_data = slot.split(':');
    let drink = drinks[parseInt(drink_data[0])];
    let value = parseInt(drink_data[1]);
    drinkDict[drink] = value;
  }

  return drinkDict;
}

function updateCounters()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      var drinkDict = parseStatus(this.responseText);
      for (let drink in drinkDict) {
        setData(drink, drinkDict[drink]);
      }
    }
  };
  xhttp.open("GET", "status", true);
  xhttp.send();
}
