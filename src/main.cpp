#include <Arduino.h>
#ifdef ESP32
#include <WiFi.h>
#include <AsyncTCP.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#include <ESPAsyncTCP.h>
#endif
// Webserver-specific #includes:
#include <ESPAsyncWebServer.h>
#include "FS.h"
#include <LittleFS.h>

// Global vars
const char* ssid = "SSID";
const char* password = "PSWD";

// ?
const char* PARAM_MESSAGE = "message";

// Sensor-specific #includes:
#include <Bme280.h>

// Initial distance status
String status = "0:000;1:000;2:000;3:000;4:000;5:000;6:000";

// Initial temperature
String temp = "0.00";

// Webserver
// README: https://github.com/me-no-dev/ESPAsyncWebServer/blob/master/README.md
AsyncWebServer server(80);

// Adafruit_BMP280 bmp280;
Bme280TwoWire sensor;

void notFound(AsyncWebServerRequest *request) {
    request->send(404, "text/plain", "Not found");
}

void setup() {

    Serial.begin(115200);
    
    // Initialize SPIFFS
    if(!LittleFS.begin()){
      Serial.println("An Error has occurred while mounting LittleFS");
      return;
    }

    // Connect to Wi-FI
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    if (WiFi.waitForConnectResult() != WL_CONNECTED) {
        Serial.printf("WiFi Failed!\n");
        return;
    }

    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());

    // Connect to bmp280
    Wire.begin(D2, D1);
    sensor.begin(Bme280TwoWireAddress::Primary);
    sensor.setSettings(Bme280Settings::indoor());
    

    // Configure webserver
    server.on("/status", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200, "text/plain", status);
    });

    server.on("/temp", HTTP_GET, [](AsyncWebServerRequest *request){
      request->send(200, "text/plain", temp);
    });

    server.serveStatic("/", LittleFS, "/").setDefaultFile("Bierautomat.html");

    server.onNotFound(notFound);

    server.begin();
}

void loop() {
  String wire_in;

  if (Serial.available()) {
    // Get new distance values
    wire_in = Serial.readStringUntil('\n');
    status = wire_in;
    Serial.println(wire_in);    
  }
  
  // Get temp 
  temp = String(sensor.getTemperature()); 
  Serial.println(temp + " °C");
  
  delay(500);
}